Binary Studio Academy PHP 2018
====


### COMMENTS
Привет, тесты не проходит, потому что ты изменил тест, так делать нельзя, если видишь, что что-то не соответствует заданию, то пишешь нам и мы либо исправляем тест либо объясняем, как под него подстроится. Твое исправление не грубое, так что за ошибку считаться не будет, но можно было попробовать изменить пространство имен для модели пользователя, для этого достаточно было в config/auth.php изменить расположение и в фабрике UserFactory подправить. Можно поиском по проекту воспользоваться и найти все вхождения, их немного.

Необязательные параметры лучше переносить в конец списка, тогда при вызове тебе не придется указывать его (SaveUserRequest.php).

Смотри, в этом задании ты сделал очень неэффективно (WalletService). Нужно было попробовать поиграться с отношениями, в данном случае идеальным отношением было бы многие ко многим, можно было в модели Wallet сделать вот так:

    public function currencies()
    {
        return $this->belongsToMany(Currency::class, 'money', 'wallet_id', 'currency_id');
    }
Причем 'wallet_id', 'currency_id' - необязательны, это для наглядности. И потом в сервисе просто выбрать всю валюту из кошелька:

    public function findCurrencies(int $walletId): Collection
    {
        return Wallet::find($walletId)->currencies()->get();
    }
Eloquent предоставляет очень удобный механизм работы с отношениями, так что я рекомендую просмотреть доки.



### Домашнее задание Laravel 5 & databases interaction 

#### Требования

Ознакомиться с migrations, seeds, Eloquent ORM & models. 

#### Установка

Установка показана в рабочем окружении OS Linux:

```
git clone git@github.com:BinaryStudioAcademy/bsa-2018-php-7.git
cd project-dir
composer install
cp .env.example .env
php artisan key:generate
```

#### Задание 1
Создать миграции которые создадут таблицы currency, money, wallet, user.
Создать классы сиды(seeds) для таблиц currency, user, wallet, money используя factories и пакет Faker. 

Схема:

| currency ||
| ---------- | ------------- |
| id | int | 
| name | string(unique) |


| money ||
| ---------- | ------------- |
| id | int | 
| currency_id | int |
| amount | float |
| wallet_id | int |
| deleted | eloquent soft delete flag |


| wallet | |
| ---------- | ------------- |
| id | int |
| user_id | int |
| deleted | eloquent soft delete flag |


| user | |
| ---------- | ------------- |
| id | int |
| name | varchar (100) |
| email | varchar (100) |


Реализовать классы - Eloquent модели в папке app/Entity, которые будут описывать соответствующие таблицы и связи между ними(hasOne, hasMany, etc).

#### Задание 0

Реализовать интерфейс CurrencyServiceInterface.

**create** - создает currency модель.

#### Задание 1

Реализовать интерфейс UserServiceInterface.

**findAll** - возвращает коллекцию всех пользователей

**findById** - возвращает пользователя по ID, если пользователя с таким идентификатором не существует - возвращает NULL

**save** - создает/обновляет запись пользователя

**delete** - удаляет запись пользователя по ID из таблицы, при удалении должен также
удаляться кошелек(wallet), привязанный к этому пользователю

#### Задание 2

Реализовать интерфейс WalletServiceInterface. 

**findByUser** - возвращает запись кошелька конкретного польователя 

**create** - создает кошелек для пользователя. Не может существовать больше 1 записи с тем же user_id.

**findCurrencies** - возвращает коллекцию currency с которыми связан кошелек. 

#### Задание 3

Реализовать интерфейс MoneyServiceInterface.

**create** - создает money модель.

**maxAmount** - возвращает float значение, наибольшее кол-во валюты в хранилище.

#### Запуск тестов

```
./vendor/bin/phpunit
```