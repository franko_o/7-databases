<?php

namespace App\Services;

use App\Entity\Currency;
use App\Entity\Money;
use App\Entity\Wallet;
use App\Requests\CreateWalletRequest;
use Illuminate\Support\Collection;

class WalletService implements WalletServiceInterface
{

    public function findByUser(int $userId): ?Wallet
    {
        return Wallet::user($userId)->first();
    }

    public function create(CreateWalletRequest $request): Wallet
    {
        $current = Wallet::user($request->getUserId())->first();
        if ($current) {
            throw new \LogicException();
        }

        $wallet = new Wallet();
        $wallet->user_id = $request->getUserId();
        $wallet->save();

        return $wallet;
    }

    public function findCurrencies(int $walletId): Collection
    {
        $money = Money::where('wallet_id', $walletId)->pluck('currency_id')->toArray();
        return Currency::find($money);
    }
}