<?php

namespace App\Services;

use App\Entity\Wallet;
use App\Requests\SaveUserRequest;
use App\User;
use Illuminate\Support\Collection;

class UserService implements UserServiceInterface
{

    public function findAll(): Collection
    {
        return User::all();
    }

    public function delete(int $id): void
    {
        Wallet::user($id)->delete();
        User::destroy($id);
    }

    public function findById(int $id): ?User
    {
        return User::find($id);
    }

    public function save(SaveUserRequest $request): User
    {
        if ($request->getId()) {
            $user = User::find($request->getId());
        } else {
            $user = new User();
        }
        $user->name = $request->getName();
        $user->email = $request->getEmail();
        $user->save();

        return $user;
    }
}