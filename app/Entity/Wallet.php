<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model
{
    use SoftDeletes;

    protected $table = 'wallet';
    protected $fillable = ['user_id'];
    protected $dates = ['deleted_at'];

    public function userRelation()
    {
        return $this->belongsTo('App\Entity\User', 'user_id', 'id');
    }

    public function scopeUser($query, $userId)
    {
        return $query->where('user_id', $userId);
    }
}
