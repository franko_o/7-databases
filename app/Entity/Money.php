<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Money extends Model
{
    use SoftDeletes;

    protected $table = 'money';
    protected $fillable = ['currency_id', 'amount', 'wallet_id'];
    protected $dates = ['deleted_at'];
}
