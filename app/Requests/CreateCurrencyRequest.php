<?php

namespace App\Requests;

use App\Entity\Currency;

class CreateCurrencyRequest
{
    /**
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}