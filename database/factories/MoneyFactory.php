<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Entity\Money::class, function (Faker $faker) {
    return [
        'currency_id' => $faker->numberBetween(1, App\Entity\Currency::count()),
        'amount' => $faker->numberBetween(1, 10000),
        'wallet_id' => $faker->numberBetween(1, App\Entity\Wallet::count())
    ];
});
